app.directive('velocityTransition', function() {
    return {
        restrict: 'A',
        scope: {
            ops: '=velocityTransition'
        },
        link: function(scope, elem, attrs) {
            if (scope.ops.delay) {
                elem.css({ opacity: 0 });
            }

            var duration = (scope.ops.duration)?scope.ops.duration:700;

            elem.velocity("transition." + scope.ops.transition, {
                delay: scope.ops.delay,
                duration: duration,
                complete: function(elements) {
                    if (scope.ops.fadeOut) {
                        $(elements).velocity("transition.fadeOut", {
                            delay: scope.ops.fadeOut
                        });
                    }
                }
            });
        }
    };
});

app.directive('stickHeader', ['$window', function($window) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            elem.css({
                top: $('.page-header').outerHeight() + "px"
            });
        }
    };
}]);

app.directive('setItemHeight', ['$window', function($window) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var heightInPixel = Math.round(($(window).height() - $('.page-header').outerHeight()) / 5) + "px";
            elem.css({
                height: heightInPixel,
                lineHeight: heightInPixel
            });
        }
    };
}]);

app.directive('centerOnParent', function() {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var elemHeight = $(elem).outerHeight();
            var parentHeight = $(elem).parent().outerHeight();
            var margin = Math.round((parentHeight - elemHeight) / 2) + "px";
            elem.css({
                marginTop: margin
            });
        }
    };
});

app.directive('toggleBtn', ['$rootScope', function($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            check: '=',
            modal: '='
        },
        link: function (scope, elem, attrs) {

            scope.status = scope.check.status;

            scope.check.previousStatus = scope.check.status;

            scope.selector = function(index) {

                $rootScope.$broadcast('checkChange', true);

                // On affiche ou non les modals
                if (index == '1' && scope.status == '0') {
                    scope.modal.open(scope.check);
                } else if (index == '0') {
                    scope.modal.open(scope.check);
                }

                scope.check.previousStatus = scope.check.status;

                // on met à jour le check à la liste
                scope.check.status = index;

                // on met visuellement à jour la checkbox
                scope.status = index;

            };

            scope.$watch('check.status', function() {
                scope.status = scope.check.status;
            });

        },
        template: '<div class="toggle-container">' +
        '<div ng-click="selector(1)" ng-class="{active: status == 1}" class="toggle-btn toggle-on"><i class="fa fa-check-circle"></i></div>' +
        '<div ng-click="selector(0)" ng-class="{active: status == 0}" class="toggle-btn toggle-off"><i class="fa fa-times-circle"></i></div>' +
        '</div>'
    };
}]);

app.directive('flashMessage', ['$timeout', function($timeout) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function(scope, elem, attrs) {
            scope.isFlashMessage = false;
            scope.message = "";

            scope.$watch('message', function() {
                if (scope.message !== "") {
                    scope.isFlashMessage = true;

                    $timeout(function() {
                        scope.isFlashMessage = false;
                    }, 1500);
                    $timeout(function() {
                        scope.message = "";
                    }, 2000);
                }
            });
        },
        template: '<div class="overlay-message-container" ng-show="isFlashMessage" ng-click="isFlashMessage = false">' +
        '<div class="overlay-message"><div class="overlay-icon"><i class="fa fa-check-square-o"></i></div>{{message}}</div>' +
        '</div>'
    };
}]);

app.directive('fileUpload', function () {
    return {
        scope: true,
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                for (var i = 0;i<files.length;i++) {
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});