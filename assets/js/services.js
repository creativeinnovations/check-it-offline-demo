app.factory("AuthService", ['$http', '$q', '$window', '$rootScope', function($http, $q, $window, $rootScope) {

    var shopInfo = null;

    if ($window.sessionStorage.shopInfo) {
        shopInfo = JSON.parse($window.sessionStorage.shopInfo);

        // Si l'utilisateur est connecté, on envoi l'event
        $rootScope.userIsConnected = true;
    }

    function getShopInfo() {
        return shopInfo;
    }

    function getShopID() {
        if (shopInfo) {
            return parseInt(shopInfo.shop.id);
        } else {
            return null;
        }
    }

    function getShopToken() {
        if (shopInfo) {
            return shopInfo.token;
        } else {
            return null;
        }
        return shopInfo.token;
    }

    function isShopManager() {
        if (shopInfo) {
            return parseInt(shopInfo.shop.is_manager);
        } else {
            return null;
        }
    }

    function isNoCheck() {
        if (shopInfo) {
            return parseInt(shopInfo.shop.is_nocheck);
        } else {
            return null;
        }
    }

    function check(identFT, code, remember) {
        var deferred = $q.defer();

        $http({
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: apiURL + "/shops/check.json",
            method: "POST",
            data: $.param({
                ident_ft: identFT,
                token: code,
                remember: remember
            })
        }).then(function(result) {

            // récupération des infos
            shopInfo = {
                token: result.data.token,
                shop: result.data.shop
            };
            // enregistrement des infos (+ token) en session storage
            $window.sessionStorage.shopInfo = JSON.stringify(shopInfo);

            // Si l'utilisateur est connecté, on envoi l'event
            $rootScope.$broadcast('userIsConnected');

            deferred.resolve(shopInfo);

        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    function cookieCheck() {
        var deferred = $q.defer();

        $http({
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: apiURL + "/shops/cookie_check.json",
            method: "GET"
        }).then(function(result) {

            // récupération des infos
            shopInfo = {
                token: result.data.token,
                shop: result.data.shop
            };
            // enregistrement des infos (+ token) en session storage
            $window.sessionStorage.shopInfo = JSON.stringify(shopInfo);

            // Si l'utilisateur est connecté, on envoi l'event
            $rootScope.$broadcast('userIsConnected');

            deferred.resolve(shopInfo);

        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    function logout() {
        var deferred = $q.defer();

        $http({
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: apiURL + "/shops/logout.json",
            method: "POST",
            data: $.param({
                token: shopInfo.token
            })
        }).then(function(result) {
            // on efface la session et la variable
            $window.sessionStorage.shopInfo = null;
            shopInfo = null;

            deferred.resolve(shopInfo);
        }, function(error) {
            // on efface la session et la variable
            $window.sessionStorage.shopInfo = null;
            shopInfo = null;

            deferred.reject(error);
        });

        return deferred.promise;
    }

    return {
        getShopInfo: getShopInfo,
        getShopID: getShopID,
        getShopToken: getShopToken,
        isNoCheck: isNoCheck,
        isShopManager: isShopManager,
        check: check,
        cookieCheck: cookieCheck,
        logout: logout
    };

}]);

app.factory('SpacesFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getSpaces: function() {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/spaces/get.json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        },
        getSpace: function(id) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/spaces/get/" + id + ".json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('CategoriesFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getCategories: function() {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/categories/get.json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        },
        getCategory: function(id) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/categories/get/" + id + ".json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('CheckingsFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getPercentDone: function(id) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/checkings/done/" + id + ".json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        },
        saveCheckings: function(values) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/checkings/save.json",
                method: "POST",
                data: $.param(values)
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('ReportsFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        sendReport: function(id) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/reports/send/" + id + ".json",
                method: "POST"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('ReportProblemFactory', ['$http', '$q', 'AuthService', '$log', function ($http, $q, AuthService, $log) {

    return {

        sendEmail: function(values) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': undefined,
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/report_problem/send.json",
                method: "POST",
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("shop_id", angular.toJson(data.shop_id));
                    formData.append("problem", angular.toJson(data.problem));
                    formData.append("floor", angular.toJson(data.floor));
                    formData.append("userfile", data.userfile);
                    return formData;
                },
                data: {
                    shop_id: values.shop_id,
                    problem: values.problem,
                    floor: values.floor,
                    userfile: values.userfile
                },
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('WeatherFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getWeather: function(id) {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/shops/weather/" + id + ".json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('SupportsFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getSupports: function() {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/supports/get.json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);

app.factory('ShopAreasFactory', ['$http', '$q', 'AuthService', function ($http, $q, AuthService) {

    return {

        getAll: function() {

            var deferred = $q.defer();

            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'G-Auth-Token': AuthService.getShopToken()
                },
                url: apiURL + "/shop-areas/get.json",
                method: "GET"
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

    };

}]);