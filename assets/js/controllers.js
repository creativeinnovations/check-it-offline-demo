app.controller('WebAppCtrl', ['$scope', '$location', '$sce', '$translate', function($scope, $location, $sce, $translate) {

    var vm = this;

    vm.showHeaderMenu = false;

    vm.toggleHeaderMenu = function() {
        vm.showHeaderMenu = !vm.showHeaderMenu;
    };

    vm.isHome = function() {
        return $location.path() === '/';
    };

    vm.goTo = function(url) {
        vm.showHeaderMenu = false;
        $location.url(url);
    };

    vm.trustHTML = function(html) {
        return $sce.trustAsHtml(html);
    };

    vm.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    };

    vm.proceduresList = [
        {
            name: "Boucle animée",
            description: "Installation / Désinstallation",
            file: "inovapp-orange.html"
        },
        {
            name: "Le Vélo",
            description: "Relancer le dispositif",
            file: "velo-relancer.html"
        }
    ];

    vm.langs = {
        "fr_FR": {
            flag: "fr",
            momentLocale: "fr"
        },
        "en_GB": {
            flag: "gb",
            momentLocale: "en-gb"
        },
        "de_DE": {
            flag: "de",
            momentLocale: "de"
        },
        "nl_NL": {
            flag: "nl",
            momentLocale: "nl"
        },
        "es_ES": {
            flag: "es",
            momentLocale: "es"
        },
        "pt_PT": {
            flag: "pt",
            momentLocale: "pt"
        },
        "it_IT": {
            flag: "it",
            momentLocale: "it"
        },
        "ru_RU": {
            flag: "ru",
            momentLocale: "ru"
        },
        "in_IN": {
            flag: "in",
            momentLocale: "hi"
        },
        "cn_CN": {
            flag: "cn",
            momentLocale: "zh-cn"
        },
        "jp_JP": {
            flag: "jp",
            momentLocale: "ja"
        },
        "kr_KR": {
            flag: "kr",
            momentLocale: "ko"
        }
    };

    vm.langSelector = false;
    vm.selectedLang = $translate.proposedLanguage();
    moment.locale(vm.langs[vm.selectedLang].momentLocale);

    vm.selectLang = function(key) {
        $translate.use(key);
        vm.selectedLang = key;
        vm.langSelector = false;
        moment.locale(vm.langs[key].momentLocale);
    };

    vm.headerMenuItems = [
        {
            link: '/spaces',
            icon: 'cubes'
        },
        {
            link: '/categories',
            icon: 'tags'
        },
        {
            link: '/weather',
            icon: 'sun'
        },
        {
            link: '/reportProblem',
            icon: 'exclamation'
        },
        {
            link: '/emergency',
            icon: 'phone'
        },
    ];

}]);

app.controller('HomeCtrl', ['$scope', '$location', '$timeout', '$translate', 'AuthService', function($scope, $location, $timeout, $translate, AuthService) {

    var vm = this;

    $scope.logoIntro = true;

    $timeout(function() {
        $scope.logoIntro = false;
    }, 2000);

    $scope.remember = true;

    $scope.httpProcessing = false;

    $scope.check = function() {

        // on rend le formulaire inaccessible pendant la verification
        $scope.httpProcessing = true;

        // verification avec l'api
        AuthService.check($scope.identFT, $scope.code, $scope.remember).then(
            function(data) {
                $location.url('/menu');
            },
            function(error) {
                // connexion ERREUR
                if (error.status == 400) {
                    $translate('HOME_EMPTY_FORM').then(function(translation) {
                        $scope.formMessage = translation;
                    }, function(translationId) {
                        $scope.formMessage = translationId;
                    });
                } else if (error.status == 401) {
                    $translate('HOME_MSG_LOGIN_FAILED').then(function(translation) {
                        $scope.formMessage = translation;
                    }, function(translationId) {
                        $scope.formMessage = translationId;
                    });
                }

                $scope.httpProcessing = false;
            }
        );
    };

    vm.cookieCheck = function() {

        // on rend le formulaire inaccessible pendant la verification d'un cookie
        $scope.httpProcessing = true;

        // verification avec l'api
        AuthService.cookieCheck().then(
            function(data) {
                $location.url('/menu');
            },
            function(error) {
                $scope.httpProcessing = false;
            }
        );
    };

    if (AuthService.getShopInfo()) {
        $location.url('/menu');
    } else {
        //vm.cookieCheck();
    }

}]);

app.controller('MenuCtrl', ['$scope', '$location', 'CheckingsFactory', 'AuthService', function($scope, $location, CheckingsFactory, AuthService) {

    var vm = this;

    $scope.app.showHeaderMenu = false;

    // ------------------------------------------------------------------------
    // Variables
    // ------------------------------------------------------------------------

    // on vérifie si l'utilisauter connecté est un RB
    $scope.isManager = AuthService.isShopManager();

    // ID de la boutique
    $scope.shopID = AuthService.getShopID();

    // infos de la boutique
    $scope.shopInfo = AuthService.getShopInfo();

    // Progression en cours
    $scope.checkProgress = '...';

    // graph
    $scope.chartOptions =  {
        responsive: false,
        maintainAspectRatio: true,
        legend: {
            display: false
        },
        cutoutPercentage: 70,
        animation: {
            numSteps: 100,
            easing: 'easeOutBounce',
            animateRotate: false,
            animateScale: false
        },
        elements: {
            arc: {
                borderWidth: 1
            }
        }
    };

    // menu config
    $scope.show_checks = parseInt($scope.shopInfo.shop.show_checks);
    $scope.show_spaces = parseInt($scope.shopInfo.shop.show_spaces);
    $scope.show_categories = parseInt($scope.shopInfo.shop.show_categories);
    $scope.no_check = parseInt($scope.shopInfo.shop.no_check);

    if ($scope.no_check > 0) {
        $scope.show_checks = 0;
    }

    // ------------------------------------------------------------------------
    // Fonctions
    // ------------------------------------------------------------------------

    // récupération des espaces
    vm.getPercentDone = function(id) {
        $scope.httpProcessing = true;

        CheckingsFactory.getPercentDone(id).then(
            function(result) {
                if (result.data.percent >= 0) {
                    $scope.checkProgress = result.data.percent + "%";
                } else {
                    $scope.checkProgress = "?";
                }
                $scope.chartProgress = result.data.chart;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };


    vm.logoutMenuOpened = false;

    vm.logout = function() {
        $scope.httpProcessing = true;

        AuthService.logout().then(
            function(data) {
                $scope.httpProcessing = true;
                $location.url('/');
            },
            function(error) {
                $scope.httpProcessing = true;
                $location.url('/');
            }
        );
    };

    vm.getPercentDone($scope.shopID);

}]);

app.controller('SpacesCtrl', ['$scope', '$location', '$window', 'SpacesFactory', 'ShopAreasFactory', function($scope, $location, $window, SpacesFactory, ShopAreasFactory) {

    var vm = this;

    $scope.chartOptions =  {
        responsive: false,
        maintainAspectRatio: true,
        legend: {
            display: false
        },
        cutoutPercentage: 30,
        animation: {
            numSteps: 100,
            easing: 'easeOutBounce',
            animateRotate: false,
            animateScale: false
        },
        elements: {
            arc: {
                borderWidth: 1,
                borderColor: '#111'
            }
        }
    };

    $scope.showProgress = ($window.sessionStorage.showProgress && $window.sessionStorage.showProgress == "false") ? false : true;

    $scope.switchProgress = function() {
        $scope.showProgress = !$scope.showProgress;

        $window.sessionStorage.showProgress = $scope.showProgress;
    };

    // récupération des espaces
    vm.getSpaces = function() {
        $scope.httpProcessing = true;

        SpacesFactory.getSpaces().then(
            function(result) {
                $scope.spacesList = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getSpaces();

    $scope.areasList = [];

    // récupération des espaces
    vm.getShopAreas = function() {
        $scope.httpProcessing = true;

        ShopAreasFactory.getAll().then(
            function(result) {
                $scope.areasList = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getShopAreas();

    // sélection d'un espace
    $scope.select = function(id) {
        $location.url('spaces/' + id);
    };

}]);

app.controller('SpaceCtrl', ['$scope', '$location', '$routeParams', '$translate', 'SpacesFactory', 'CheckingsFactory', 'ReportsFactory', 'AuthService', function($scope, $location, $routeParams, $translate, SpacesFactory, CheckingsFactory, ReportsFactory, AuthService) {

    var vm = this;

    // ------------------------------------------------------------------------
    // Variables
    // ------------------------------------------------------------------------

    // espace sélectionné
    $scope.spaceID = $routeParams.id;

    // variable permettant de définir si on est en shortlist
    $scope.shortList = undefined;

    // on vérifie si l'utilisauter connecté est un RB
    $scope.isManager = AuthService.isShopManager();

    // ID de la boutique
    $scope.shopID = AuthService.getShopID();

    // définit si des modifications ont été faites sans sauvegarder
    $scope.changeMade = false;

    // ------------------------------------------------------------------------
    // Evenements
    // ------------------------------------------------------------------------

    $scope.$on('checkChange', function() {
        $scope.changeMade = true;
    });

    // ------------------------------------------------------------------------
    // Fonctions
    // ------------------------------------------------------------------------

    // récupère la checklist
    vm.getSpace = function(id) {
        $scope.httpProcessing = true;

        SpacesFactory.getSpace(id).then(
            function(result) {
                $scope.space = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getSpace($scope.spaceID);

    // fonction permettant de switcher en shortlist
    $scope.switchList = function() {
        if ($scope.shortList === 1) {
            $scope.shortList = undefined;
        } else {
            $scope.shortList = 1;
        }
    };

    // sauvegarde de la checklist en cours
    $scope.save = function() {
        $scope.httpProcessing = true;

        // préparation du tableau de variables
        var values = [];

        angular.forEach($scope.space.categories, function(category, categoryKey) {
            angular.forEach(category.checks, function(check, checkKey) {
                values.push({
                    shop_id: $scope.shopID,
                    check_id: check.id,
                    status: check.status,
                    comments: check.comments
                });
            });
        });

        var data = {
            values: values
        };

        // sauvegarde des checks
        CheckingsFactory.saveCheckings(data).then(
            function(results) {
                $scope.thanks();
                $scope.changeMade = false;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    // envoi du rapport
    $scope.send = function() {
        $scope.httpProcessing = true;
        // sauvegarde des checks
        ReportsFactory.sendReport(AuthService.getShopID()).then(
            function(results) {
                $scope.thanks();
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    // message de remerciement
    $scope.thanks = function() {
        $translate('MSG_THANKS').then(function(translation) {
            $scope.message = translation;
        }, function(translationId) {
            $scope.message = translationId;
        });
    };

    // gestionnaire de la modal commentaires
    $scope.commentsModal = {
        curCheck: null,
        open: function(check) {
            curCheck = check;
            $scope.comments = "";
            $scope.modalOpened = true;
        },
        save: function() {
            curCheck.comments = $scope.comments;
            curCheck.previousStatus = curCheck.status;
            $scope.modalOpened = false;
            $scope.thanks();
        },
        cancel: function() {
            curCheck.status = curCheck.previousStatus;
            $scope.modalOpened = false;
        },
        thanks: function() {
            $scope.thanks();
        }
    };

}]);

app.controller('CategoriesCtrl', ['$scope', '$location', '$window', 'CategoriesFactory', function($scope, $location, $window, CategoriesFactory) {

    var vm = this;

    $scope.chartOptions =  {
        responsive: false,
        maintainAspectRatio: true,
        legend: {
            display: false
        },
        cutoutPercentage: 30,
        animation: {
            numSteps: 100,
            easing: 'easeOutBounce',
            animateRotate: false,
            animateScale: false
        },
        elements: {
            arc: {
                borderWidth: 1,
                borderColor: '#111'
            }
        }
    };

    $scope.showProgress = ($window.sessionStorage.showProgress && $window.sessionStorage.showProgress == "false") ? false : true;

    $scope.switchProgress = function() {
        $scope.showProgress = !$scope.showProgress;

        $window.sessionStorage.showProgress = $scope.showProgress;
    };

    // récupération des catégories
    vm.getCategories = function() {
        $scope.httpProcessing = true;

        CategoriesFactory.getCategories().then(
            function(result) {
                $scope.catsList = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getCategories();

    // sélection d'une catégorie
    $scope.select = function(index) {
        $location.url('categories/' + $scope.catsList[index].id);
    };

}]);

app.controller('CategoryCtrl', ['$scope', '$location', '$routeParams', '$translate', 'CategoriesFactory', 'CheckingsFactory', 'ReportsFactory', 'AuthService', function($scope, $location, $routeParams, $translate, CategoriesFactory, CheckingsFactory, ReportsFactory, AuthService) {

    var vm = this;

    // ------------------------------------------------------------------------
    // Variables
    // ------------------------------------------------------------------------

    // catégorie sélectionnée
    $scope.categoryID = $routeParams.id;

    // variable permettant de définir si on est en shortlist
    $scope.shortList = undefined;

    // on vérifie si l'utilisauter connecté est un RB
    $scope.isManager = AuthService.isShopManager();

    // ID de la boutique
    $scope.shopID = AuthService.getShopID();

    // définit si des modifications ont été faites sans sauvegarder
    $scope.changeMade = false;

    // ------------------------------------------------------------------------
    // Evenements
    // ------------------------------------------------------------------------

    $scope.$on('checkChange', function() {
        $scope.changeMade = true;
    });

    // ------------------------------------------------------------------------
    // Fonctions
    // ------------------------------------------------------------------------

    // récupère la checklist
    vm.getCategory = function(id) {
        $scope.httpProcessing = true;

        CategoriesFactory.getCategory(id).then(
            function(result) {
                $scope.category = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getCategory($scope.categoryID);

    // fonction permettant de switcher en shortlist
    $scope.switchList = function() {
        if ($scope.shortList === 1) {
            $scope.shortList = undefined;
        } else {
            $scope.shortList = 1;
        }
    };

    // sauvegarde de la checklist en cours
    $scope.save = function() {
        $scope.httpProcessing = true;

        // préparation du tableau de variables
        var values = [];

        angular.forEach($scope.category.spaces, function(space, spaceKey) {
            angular.forEach(space.checks, function(check, checkKey) {
                values.push({
                    shop_id: $scope.shopID,
                    check_id: check.id,
                    status: check.status,
                    comments: check.comments
                });
            });
        });

        var data = {
            values: values
        };

        // sauvegarde des checks
        CheckingsFactory.saveCheckings(data).then(
            function(results) {
                $scope.thanks();
                $scope.changeMade = false;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    // envoi du rapport
    $scope.send = function() {
        $scope.httpProcessing = true;
        // sauvegarde des checks
        ReportsFactory.sendReport(AuthService.getShopID()).then(
            function(results) {
                $scope.thanks();
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    // message de remerciement
    $scope.thanks = function() {
        $translate('MSG_THANKS').then(function(translation) {
            $scope.message = translation;
        }, function(translationId) {
            $scope.message = translationId;
        });
    };

    // gestionnaire de la modal commentaires
    $scope.commentsModal = {
        curCheck: null,
        open: function(check) {
            curCheck = check;
            $scope.comments = "";
            $scope.modalOpened = true;
        },
        save: function() {
            curCheck.comments = $scope.comments;
            curCheck.previousStatus = curCheck.status;
            $scope.modalOpened = false;
            $scope.thanks();
        },
        cancel: function() {
            curCheck.status = curCheck.previousStatus;
            $scope.modalOpened = false;
        },
        thanks: function() {
            $scope.thanks();
        }
    };

}]);

app.controller('ProceduresCtrl', ['$scope', '$location', '$window', function($scope, $location, $window) {

    var vm = this;

}]);

app.controller('ProcedureCtrl', ['$scope', '$location', '$window', '$routeParams', function($scope, $location, $window, $routeParams) {

    var vm = this;

    // procedure sélectionnée
    $scope.procedureID = $routeParams.id;

}]);

app.controller('WeatherCtrl', ['$scope', '$location', 'WeatherFactory', 'AuthService', function($scope, $location, WeatherFactory, AuthService) {

    var vm = this;

    // récupère la météo
    vm.getWeather = function(id) {
        $scope.httpProcessing = true;

        WeatherFactory.getWeather(id).then(
            function(result) {
                $scope.forecast = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    vm.getWeather(AuthService.getShopID());

    $scope.detailedWeather = false;

}]);

app.controller('ReportProblemCtrl', ['$scope', '$location', '$translate', 'ReportProblemFactory', 'ShopAreasFactory', 'AuthService', function($scope, $location, $translate, ReportProblemFactory, ShopAreasFactory, AuthService) {

    var vm = this;

    // ------------------------------------------------------------------------
    // Variables
    // ------------------------------------------------------------------------

    // on vérifie si l'utilisauter connecté est un RB
    $scope.isManager = AuthService.isShopManager();

    // ID de la boutique
    $scope.shopID = AuthService.getShopID();

    // Heure actuelle
    $scope.currentDateTime = moment().format('LLLL');

    // on "vide" le fichier à envoyer
    var file = null;

    $scope.$on("fileSelected", function (event, args) {
        file = args.file;
    });

    // areas
    $scope.areasList = [];
    $scope.selectedArea = null;

    // récupération des zones
    var getShopAreas = function() {
        $scope.httpProcessing = true;

        ShopAreasFactory.getAll().then(
            function(result) {
                $scope.areasList = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    getShopAreas();

    // ------------------------------------------------------------------------
    // Fonctions
    // ------------------------------------------------------------------------

    $scope.send = function() {
        $scope.httpProcessing = true;

        // préparation du tableau de variables
        var values = {
            shop_id: $scope.shopID,
            problem: $scope.problem,
            area: $scope.selectedArea || "Aucune",
            userfile: file
        };

        // sauvegarde des checks
        ReportProblemFactory.sendEmail(values).then(
            function(results) {
                $translate('MSG_PROBLEM_REPORTED').then(function(translation) {
                    $scope.message = translation;
                }, function(translationId) {
                    $scope.message = translationId;
                });
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

}]);

app.controller('EmergencyCtrl', ['$scope', '$location', 'SupportsFactory', function($scope, $location, SupportsFactory) {

    var vm = this;

    // récupération des supports
    var getSupports = function() {
        $scope.httpProcessing = true;

        SupportsFactory.getSupports().then(
            function(result) {
                $scope.supportsList = result.data;
                $scope.httpProcessing = false;
            },
            function(error) {
                $scope.error = error;
                $scope.httpProcessing = false;
            }
        );
    };

    getSupports();

}]);