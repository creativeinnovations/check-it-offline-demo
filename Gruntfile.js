module.exports = function(grunt) {

    var globalConfig = {
        defaultAssets: 'assets/',
        libs: 'assets/libs/',
        langsPath: 'assets/langs/'
    };

    grunt.initConfig({

        globalConfig: globalConfig,

        i18nextract: {
            default_options: {
                src: ['assets/js/*.js', 'partials/*.html', 'index.php'],
                lang: ['fr_FR', 'en_GB', 'de_DE', 'es_ES', 'pt_PT', 'it_IT', 'ru_RU', 'cn_CN', 'jp_JP', 'kr_KR', 'in_IN', 'nl_NL'],
                dest: '<%= globalConfig.langsPath %>'
            }
        },

        jshint: {
            default: {
                src: [
                    '<%= globalConfig.defaultAssets %>js/**/*.js',
                    '!<%= globalConfig.defaultAssets %>js/**/*.min.js'
                ]
            }
        },

        uglify: {
            options: {
                mangle: false,
                preserveComments: false,
                unused: false,
                compress: {
                    drop_console: true
                }
            },
            default: {
                files: [{
                    expand: true,
                    cwd: '<%= globalConfig.defaultAssets %>js',
                    src: ['**/*.js', '!**/*.min.js'],
                    dest: '<%= globalConfig.defaultAssets %>js',
                    ext: '.min.js'
                }]
            }
        },

        less: {
            default: {
                options: {
                    compress: true
                },
                files: {
                    '<%= globalConfig.defaultAssets %>css/app.min.css': '<%= globalConfig.defaultAssets %>less/app.less'
                }
            }
        },

        watch: {
            defaultJs: {
                files: ['<%= globalConfig.defaultAssets %>js/**/*.js'],
                tasks: ['newer:jshint', 'newer:uglify'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            defaultLess: {
                files: ['<%= globalConfig.defaultAssets %>less/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-angular-translate');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('extract', ['i18nextract']);
    grunt.registerTask('default', ['jshint', 'uglify', 'less']);

}