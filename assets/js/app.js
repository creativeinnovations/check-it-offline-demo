var app = angular.module('webapp', ['ngRoute', 'ngSanitize', 'ngAnimate', 'ngTouch', 'ngCookies', 'tc.chartjs', 'angulartics', 'angulartics.google.analytics', 'pascalprecht.translate']);

app.run(['$rootScope', '$location', function($rootScope, $location) {
    FastClick.attach(document.body);

    $rootScope.$on("$routeChangeError", function(event, current, previous, eventObj) {
        if (eventObj.authenticated === false) {
            $location.path("/");
        }
    });
}]);

app.config(['$routeProvider', '$translateProvider', function ($routeProvider, $translateProvider) {

    // chargement des langues
    $translateProvider.useStaticFilesLoader({
        prefix: 'assets/langs/',
        suffix: '.json'
    });

    // stratégie de "désinfection"
    $translateProvider.useSanitizeValueStrategy('escape');

    // paramètres des langues par défaut/fallback
    $translateProvider.preferredLanguage('fr_FR').fallbackLanguage('fr_FR');

    // sauvegarde des préférences
    $translateProvider.useCookieStorage();

    $routeProvider
        .when('/',
        {
            controller: 'HomeCtrl',
            controllerAs: 'home',
            templateUrl: 'partials/home.html?v=22112016'
        })
        .when('/menu',
        {
            controller: 'MenuCtrl',
            controllerAs: 'menu',
            templateUrl: 'partials/menu.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/spaces',
        {
            controller: 'SpacesCtrl',
            controllerAs: 'spaces',
            templateUrl: 'partials/spaces.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/spaces/:id',
        {
            controller: 'SpaceCtrl',
            controllerAs: 'space',
            templateUrl: 'partials/space.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/categories',
        {
            controller: 'CategoriesCtrl',
            controllerAs: 'categories',
            templateUrl: 'partials/categories.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/categories/:id',
        {
            controller: 'CategoryCtrl',
            controllerAs: 'category',
            templateUrl: 'partials/category.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/procedures',
        {
            controller: 'ProceduresCtrl',
            controllerAs: 'procedures',
            templateUrl: 'partials/procedures.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/procedures/:id',
        {
            controller: 'ProcedureCtrl',
            controllerAs: 'procedure',
            templateUrl: 'partials/procedure.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/weather',
        {
            controller: 'WeatherCtrl',
            controllerAs: 'weather',
            templateUrl: 'partials/weather.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/emergency',
        {
            controller: 'EmergencyCtrl',
            controllerAs: 'emergency',
            templateUrl: 'partials/emergency.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .when('/reportProblem',
        {
            controller: 'ReportProblemCtrl',
            controllerAs: 'reportProblem',
            templateUrl: 'partials/reportProblem.html?v=22112016',
            resolve: {
                auth: ["$q", "AuthService", function($q, AuthService) {
                    var shopInfo = AuthService.getShopInfo();

                    if (shopInfo) {
                        return $q.when(shopInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }
                }]
            }
        })
        .otherwise({ redirectTo: '/' });
}]);